# Develop

## Setup

### Install Ruby

The supported Ruby version can be found in the [Gemfile](./Gemfile) (we recommended using a
version manager like [rvm](https://rvm.io)).

### Install Bundler

Now that Ruby is installed, we can use its package manager `gem` to install a dependency manager:
```
gem install bundler
```

### Install MySQL 5.7

https://dev.mysql.com/doc/refman/5.7/en/installing.html

Create a user that matches the credentials in the `development` entry of
[database.yml](./config/database.yml).

## Background Tasks

Some tasks are run on a schedule or deferred for performance. In a production environment these
tasks will be managed by background processes, but in a development environment they must be run
manually:
```
rake forum_submission:delay_update_scores  # queue jobs to recalculate ForumSubmission scores
rake jobs:workoff                          # run queued Delayed::Jobs
```

### Configure bundler

The api rails app depends on a private gem hosted on GilLab. In order to install this gem, you will
need to configure bundler with a personal access token. You can do so
[here](https://gitlab.com/profile/personal_access_tokens) by creating on with the `read_repository`
scope. After creating the personal access token, run the following command to configure bundler:
```
bundle config --local gitlab.com <gitlab-username>:<person-access-token>
```

## Run

### Initialize database

Initialize the database from the schema file:
```
rake db:schema:load
```

Ensure your dependencies are up to date, then start the application at `localhost:3000`:
```
bundle install
rails server
```

# Deploy

## Server setup

### Install Docker

_Docker will also need to be installed on your local machine:_ https://docs.docker.com/install

Add the host user to the `docker` user group so the container can be started without `sudo`
privileges:
```
sudo usermod -aG docker ubuntu
```

### Configure Webroot

As the docker image initializes, it will copy files from `public/` into the container's
`/var/www/scaffold-api/` directory, which should be mounted at the selected `<webroot-path>` on the
host.

### Set Environment Variables

Create a file to define environment variables. The path will need to be passed to the `docker`
command as `<env-file-path>`:
```
RAILS_ENV=

# set these to the result of `rake secret`
SCAFFOLD_API_AUTH_KEY=
SCAFFOLD_API_SECRET_KEY_BASE=

SCAFFOLD_API_MYSQL_DB_HOST=
SCAFFOLD_API_MYSQL_DB_PORT=
SCAFFOLD_API_MYSQL_DB=
SCAFFOLD_API_MYSQL_USERNAME=
SCAFFOLD_API_MYSQL_PASSWORD=
```

## Build, run, and deploy

Build the Docker image:
```
docker build . -t registry.gitlab.com/<gitlab-username>/scaffold-core:<tag>
```

Run the Docker image and start the api server:
```
docker run \
  --env-file <env-file-path> \
  -v <webroot-path>:/var/www/scaffold-api/ \
  -p 3000:3000 \
  --entrypoint scripts/init_server.sh \
  registry.gitlab.com/<gitlab-username>/scaffold-core:<tag>
```

Run the Docker image and start the Delayed::Job worker:
```
docker run \
  --env-file <env-file-path> \
  --entrypoint scripts/init_worker.sh \
  registry.gitlab.com/<gitlab-username>/scaffold-core:<tag>
```

Push the Docker image to DockerHub:
```
docker push registry.gitlab.com/<gitlab-username>/scaffold-core:<tag>
```

Pull the Docker image:
```
docker pull registry.gitlab.com/<gitlab-username>/scaffold-core:<tag>
```
