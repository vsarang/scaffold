module V1
  class StatusController < ApplicationController
    def get
      render(
        json: {
          status: 'ok'
        }
      )
    end
  end
end
