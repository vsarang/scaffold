Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'http://localhost:8080'
    resource '*',
      headers: :any,
      methods: [:get, :post, :put, :delete, :options],
      expose: ['Content-Range'],
      credentials: true
  end
end
