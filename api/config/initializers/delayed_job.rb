class LogErrorPlugin < Delayed::Plugin
  callbacks do |lifecycle|
    lifecycle.around(:invoke_job) do |job, *args, &block|
      begin
        block.call(job, *args)
      rescue StandardError => error
        Log::Error.log_error(error)
        raise error
      end
    end
  end
end

Delayed::Worker.max_attempts = 3
Delayed::Worker.plugins << LogErrorPlugin
Delayed::Worker.logger = Logger.new(Rails.root.join('log', 'delayed_job.log'))
