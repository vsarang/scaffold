Rails.application.routes.draw do
  namespace :v1 do
    get :status, to: 'status#get'
  end
end
