#!/bin/sh
date >> public/index.html
rsync -r public/ /var/www/scaffold-api --delete

rake db:migrate:with_data
rake db:seed

whenever --user root
whenever --update-crontab
service cron start

rake jobs:work
