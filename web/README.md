# Develop

## Setup

### Install Node

This project requires Node v12 (a version manager like [n](https://www.npmjs.com/package/n) is recommended).

### Setup config

Create two files `config/development.json` and `config/production.json` with the values below
(both are needed to start the development server):

```
  {
    "ENV": {
    }
  }
```

## Run

Ensure your dependencies are up to date:
```
npm install
```

Start the development server:
```
npm start
```

You will now be able to access the UI in your web browser at `http://localhost:8080`.
