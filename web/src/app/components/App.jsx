import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import 'react-datetime/css/react-datetime.css';

import { parseParams } from 'lib/utilities/String';
import GlobalStackProvider from 'lib/components/providers/global-stack/GlobalStackProvider';
import AppConfigProvider from 'lib/components/providers/app-config/AppConfigProvider';

import { GLOBAL_STACK_MAP } from 'app/utilities/Router';
import { connectWith } from 'app/utilities/React';

import Navbar from 'app/components/nav/Navbar';
import Navdock from 'app/components/nav/Navdock';

import NotFoundPage from 'app/components/nav/NotFoundPage';

import styles from './app.scss';

const REVERSE_GLOBAL_STACK_MAP = _.invert(GLOBAL_STACK_MAP);

class ContentWrapper extends React.Component {
  parseGlobalStack() {
    const params = parseParams(this.props.location.search, { convert: true });
    const values = {};
    _.forEach(params, (value, param) => {
      const globalStackKey = REVERSE_GLOBAL_STACK_MAP[param];
      if (globalStackKey) {
        values[globalStackKey] = value;
      }
    });

    return values;
  }

  toParams(values) {
    const params = {};
    _.forEach(values, (value, key) => {
      const param = GLOBAL_STACK_MAP[key];
      if (param) {
        params[param] = value;
      } else {
        throw new Error(`${key} not found in GLOBAL_STACK_MAP`);
      }
    });
    return params;
  }

  popGlobalStack = (n = 1) => {
    this.props.enhancedRouter.goBack(n);
  }

  popGlobalStackTo = (values) => {
    const params = this.toParams(values);
    this.props.enhancedRouter.goBackTo({ params });
  }

  pushGlobalStack = (values) => {
    const params = this.toParams(values);
    this.props.enhancedRouter.push({ params });
  }

  replaceGlobalStack = (values) => {
    const params = this.toParams(values);
    this.props.enhancedRouter.replace({ params });
  }

  render() {
    return (
      <div
        className={ styles.app }
      >
        <AppConfigProvider
          filePrefix={ '' }
        >
          <GlobalStackProvider
            values={ this.parseGlobalStack() }
            pushValues={ this.pushGlobalStack }
            replaceValues={ this.replaceGlobalStack }
            pop={ this.popGlobalStack }
            popToValues={ this.popGlobalStackTo }
          >
            <div className={ styles.outerWrapper }>
              { !this.props.hideNav && <Navbar disableLogin={ this.props.disableLogin } /> }
              <div className={ styles.scrollWrapper }>
                <div className={ styles.innerWrapper }>
                  { this.props.children }
                </div>
              </div>
              <Navdock />
            </div>
          </GlobalStackProvider>
        </AppConfigProvider>
      </div>
    );
  }
}

ContentWrapper.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string.isRequired
  }).isRequired,
  enhancedRouter: PropTypes.shape({
    push: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
    goBackTo: PropTypes.func.isRequired
  }).isRequired,
  hideNav: PropTypes.bool,
  disableLogin: PropTypes.bool,
  children: PropTypes.node
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
      showMaintenancePage: false
    };
  }

  componentDidMount() {
    this.initialize();
  }

  initialize() {
    this.setState({ initialized: true });
  }

  getRoutes() {
    return [];
  }

  render() {
    if (this.state.showMaintenancePage) {
      return (
        <ContentWrapper
          disableLogin
          location={ this.props.location }
          enhancedRouter={ this.props.enhancedRouter }
        >
          <Switch>
            <Route
              path="/"
              component={ NotFoundPage }
            />
          </Switch>
        </ContentWrapper>
      );
    }

    if (!this.state.initialized) {
      return (
        <ContentWrapper
          hideNav
          location={ this.props.location }
          enhancedRouter={ this.props.enhancedRouter }
        />
      );
    }

    return (
      <ContentWrapper
        location={ this.props.location }
        enhancedRouter={ this.props.enhancedRouter }
      >
        <Switch>
          { this.getRoutes() }
          <Route
            path="/"
            component={ NotFoundPage }
          />
        </Switch>
      </ContentWrapper>
    );
  }
}

App.propTypes = {
  location: PropTypes.object.isRequired,
  enhancedRouter: PropTypes.object.isRequired
};

export default connectWith({
  withRouter: true
})(App);
