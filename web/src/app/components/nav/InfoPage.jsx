import { faChevronRight } from '@fortawesome/pro-solid-svg-icons';
import React from 'react';
import PropTypes from 'prop-types';

import Page from 'lib/components/wrappers/page/Page';
import Label from 'lib/components/wrappers/label/Label';
import Link from 'lib/components/inputs/link/Link';
import InputGroup from 'lib/components/wrappers/input/InputGroup';

import { connectWith } from 'app/utilities/React';
import { HideNavbar } from 'app/components/nav/Navbar';

import styles from './info-page.scss';

class InfoPage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <HideNavbar />
        <Page
          center
          padded={ false }
        >
          <div className={ styles.header }>
            <Link
              target={ '/' }
              descriptor={ 'homePage' }
            >
              { 'nav.title' }
            </Link>
            {
              <InputGroup
                alignRight
              >
                <Link
                  light
                  plain
                  descriptor={ 'navToHomePage' }
                  target={ '/' }
                >
                  <Label
                    small
                    reverse
                    icon={ faChevronRight }
                  >
                    { this.props.copy.get('nav.home.label') }
                  </Label>
                </Link>
              </InputGroup>
            }
          </div>
          <div className={ styles.content }>
            { this.props.children }
          </div>
        </Page>
      </React.Fragment>
    );
  }
}

InfoPage.propTypes = {
  copy: PropTypes.shape({
    get: PropTypes.func.isRequired
  }).isRequired,
  center: PropTypes.bool,
  padded: PropTypes.bool,
  children: PropTypes.node
};

export default connectWith({
  withCopy: true
})(InfoPage);
