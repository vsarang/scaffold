import PropTypes from 'prop-types';
import React from 'react';

import Page from 'lib/components/wrappers/page/Page';
import Img from 'lib/components/wrappers/img/Img';

import { connectWith } from 'app/utilities/React';
import { HideNavbar } from 'app/components/nav/Navbar';

import styles from './lockout-page.scss';

const LockoutPage = (props) => (
  <React.Fragment>
    <HideNavbar />
    <Page
      center
      compact
      internalMargins
    >
        <div className={ styles.imageWrapper }>
          <Img
            transparent
            aspect={ 1067/557 }
            placeholder={ null }
            src={ '/logo.png' }
          />
        </div>
        { props.children }
    </Page>
  </React.Fragment>
);

LockoutPage.propTypes = {
  copy: PropTypes.shape({
    get: PropTypes.func.isRequired
  }).isRequired,

  title: PropTypes.string,
  internalMargins: PropTypes.bool,
  center: PropTypes.bool,
  children: PropTypes.node
};

export default connectWith({
  withCopy: true
})(LockoutPage);
