import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

import { PortalTarget, PortalContent } from 'lib/components/providers/portal/PortalZoneProvider';
import { withCopy } from 'lib/components/providers/copy/CopyProvider';
import { withEnhancedRouter } from 'lib/components/providers/enhanced-router/EnhancedRouterProvider';
import ControlBar from 'lib/components/wrappers/control/ControlBar';
import Link from 'lib/components/inputs/link/Link';

import styles from './navbar.scss';

const PORTAL_ID = 'navbar';
const LEFT_CONTENT_PORTAL_ID = 'navbar-left-content';
const RIGHT_CONTENT_PORTAL_ID = 'navbar-right-content';
const CONTENT_PORTAL_ID = 'navbar-content';

class Navbar extends React.Component {
  render() {
    return (
      <PortalTarget
        id={ PORTAL_ID }
        defaultContent={
          <ControlBar
            primary
            borderBottom
            leftContent={
              <div className={ styles.contentWrapper }>
                <PortalTarget
                  id={ LEFT_CONTENT_PORTAL_ID }
                />
                <PortalTarget
                  id={ CONTENT_PORTAL_ID }
                  defaultContent={
                    <Link
                      branded
                      large
                      light
                      noWrap
                      target={ '/' }
                    >
                      { this.props.copy.get('nav.title') }
                    </Link>
                  }
                />
              </div>
            }
            rightContent={
              <div className={ styles.contentWrapper }>
                <PortalTarget
                  id={ RIGHT_CONTENT_PORTAL_ID }
                />
              </div>
            }
          />
        }
      />
    );
  }
}

Navbar.propTypes = {
  copy: PropTypes.shape({
    get: PropTypes.func.isRequired
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
    search: PropTypes.string.isRequired
  }).isRequired,
  enhancedRouter: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  navbarContent: PropTypes.node
};

export const HideNavbar = () => (
  <PortalContent id={ PORTAL_ID }>
    { null }
  </PortalContent>
);

HideNavbar.propTypes = {
  children: PropTypes.node
};

export const NavbarLeftContent = (props) => (
  <PortalContent id={ LEFT_CONTENT_PORTAL_ID }>
    { props.children }
  </PortalContent>
);

NavbarLeftContent.propTypes = {
  children: PropTypes.node
};

export const NavbarContent = (props) => (
  <PortalContent id={ CONTENT_PORTAL_ID }>
    { props.children }
  </PortalContent>
);

NavbarContent.propTypes = {
  children: PropTypes.node
};

export const NavbarRightContent = (props) => (
  <PortalContent id={ RIGHT_CONTENT_PORTAL_ID }>
    { props.children }
  </PortalContent>
);

NavbarRightContent.propTypes = {
  children: PropTypes.node
};

export default compose(
  withCopy,
  withRouter,
  withEnhancedRouter
)(Navbar);
