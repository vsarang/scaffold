import PropTypes from 'prop-types';
import React from 'react';

import ControlBar from 'lib/components/wrappers/control/ControlBar';
import { PortalTarget, PortalContent } from 'lib/components/providers/portal/PortalZoneProvider';

import styles from './navdock.scss';

const TITLE_PORTAL_ID = 'navdock-title';
const SUBTITLE_PORTAL_ID = 'navdock-subtitle';
const ACTION_PORTAL_ID = 'navdock-action';

const Navdock = () => (
  <ControlBar borderTop>
    <div className={ styles.navdockContent }>
      <div className={ styles.leftContent }>
        <PortalTarget id={ TITLE_PORTAL_ID } />
        <PortalTarget id={ SUBTITLE_PORTAL_ID } />
      </div>
      <PortalTarget id={ ACTION_PORTAL_ID } />
    </div>
  </ControlBar>
);

export const NavdockContent = (props) => {
  return (
    <React.Fragment>
      {
        props.title && (
          <PortalContent id={ TITLE_PORTAL_ID }>
            <div className={ styles.title }>{ props.title }</div>
          </PortalContent>
        )
      }
      {
        props.subtitle && (
          <PortalContent id={ SUBTITLE_PORTAL_ID }>
            <div className={ styles.subtitle }>{ props.subtitle }</div>
          </PortalContent>
        )
      }
      {
        props.action && (
          <PortalContent id={ ACTION_PORTAL_ID }>
            <div className={ styles.action }>{ props.action }</div>
          </PortalContent>
        )
      }
    </React.Fragment>
  );
};

NavdockContent.propTypes = {
  title: PropTypes.node,
  subtitle: PropTypes.node,
  action: PropTypes.node
};

export default Navdock;
