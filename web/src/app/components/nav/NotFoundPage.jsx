import React from 'react';
import LockoutPage from 'app/components/nav/LockoutPage';

const NotFoundPage = () => {
  return (
    <LockoutPage>
      { 'Not found' }
    </LockoutPage>
  );
};

export default NotFoundPage;
