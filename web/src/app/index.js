import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import CopyProvider from 'lib/components/providers/copy/CopyProvider';
import EnhancedRouterProvider from 'lib/components/providers/enhanced-router/EnhancedRouterProvider';
import BreakpointsProvider from 'lib/components/providers/breakpoints/BreakpointsProvider';
import PortalZoneProvider from 'lib/components/providers/portal/PortalZoneProvider';

import COPY from 'app/copy.json';
import App from 'app/components/App';
import store from 'app/store';

const AppEnv = () => (
  <Provider store={ store }>
    <BrowserRouter>
      <EnhancedRouterProvider
        paramNamespace={
          {
          }
        }
      >
        <CopyProvider config={ COPY }>
          <BreakpointsProvider breakpoints={ { mobile: 50 } }>
            <PortalZoneProvider>
              <App />
            </PortalZoneProvider>
          </BreakpointsProvider>
        </CopyProvider>
      </EnhancedRouterProvider>
    </BrowserRouter>
  </Provider>
);

render(<AppEnv />, document.getElementById('root'));
