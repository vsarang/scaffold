import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

import {
  withApiModelClient
} from 'lib/components/providers/api-model-client/ApiModelClientProvider';
import {
  withApiModelProvider,
  withApiModels
} from 'lib/components/providers/api-model/ApiModelProvider';
import {
  withImageClient
} from 'lib/components/providers/image-client/ImageClientProvider';
import { withBreakpoints } from 'lib/components/providers/breakpoints/BreakpointsProvider';
import { withCopy } from 'lib/components/providers/copy/CopyProvider';
import { withEnhancedRouter } from
  'lib/components/providers/enhanced-router/EnhancedRouterProvider';
import { withGlobalStack } from 'lib/components/providers/global-stack/GlobalStackProvider';
import { withAppConfig } from 'lib/components/providers/app-config/AppConfigProvider';

export const connectWith = (options) => {
  return (Component) => {
    const hocs = _.compact([
      options.withImageClient && withImageClient,
      options.withApiModelClient && withApiModelClient,
      options.withBreakpoints && withBreakpoints,
      options.withCopy && withCopy,
      options.withGlobalStack && withGlobalStack,
      options.withAppConfig && withAppConfig,
      options.withRouter && withEnhancedRouter,
      options.withRouter && withRouter,
      (
        options.mapStateToProps || options.mapDispatchToProps
      ) && connect(options.mapStateToProps, options.mapDispatchToProps),
      options.withApiModelProvider && withApiModelProvider(
        options.withApiModelProvider
      ),
      options.withApiModels && withApiModels(options.withApiModels)
    ]);

    return compose(...hocs)(Component);
  }
};
