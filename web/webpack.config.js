const CopyPlugin = require('copy-webpack-plugin');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const childProcess = require('child_process');

module.exports = function({ env = 'production' } = {}) {
  const outputPath = path.join(__dirname, 'dist');
  const config = JSON.parse(fs.readFileSync(path.join(__dirname, `config/${env}.json`)));
  const prodConfig = JSON.parse(fs.readFileSync(path.join(__dirname, `config/production.json`)));

  const version = childProcess.execSync('git describe --tags').toString().trim();
  config.ENV.VERSION = JSON.stringify(version);

  const ENV = {};
  const globals = {};
  const globalKeys = Object.keys(prodConfig.ENV).concat(Object.keys(config.ENV));
  for (const globalKey of globalKeys) {
    const value = config.ENV[globalKey] === undefined ?
      prodConfig.ENV[globalKey] :
      config.ENV[globalKey];

    ENV[globalKey] = value;
    globals[`ENV.${globalKey}`] = JSON.stringify(value);
  }
  console.log(globals);

  return {
    mode: env,
    entry: './src/app/index.js',
    output: {
      path: outputPath,
      filename: `${version}.[contenthash].js`,
      publicPath: '/'
    },
    module: {
      rules: [{
        test: /\.jsx?$/,
        include: path.join(__dirname, 'src'),
        use: 'babel-loader'
      }, {
        test: /\.scss$/,
        include: path.join(__dirname, 'src'),
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: 'dashesOnly',
              sourceMap: false,
              localIdentName: env === 'development' ?
                '[name]-[local]-[hash:base64:5]' :
                null
            }
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              data: '@import \'app/components/index.scss\';',
              includePaths: [
                path.join(__dirname, 'src')
              ]
            }
          }
        ]
      }, {
        test: /\.css$/,
        include: path.join(__dirname),
        use: [
          'style-loader',
          'css-loader'
        ]
      }, {
        test: /\.ttf$/,
        include: path.join(__dirname, 'src/lib/components/styles/fonts'),
        use: 'file-loader'
      }]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        path.join(__dirname, 'src'),
        'node_modules'
      ]
    },
    devServer: {
      contentBase: outputPath,
      compress: true,
      port: 8080,
      historyApiFallback: true
    },
    devtool: 'source-map',
    plugins: [
      new webpack.DefinePlugin(globals),
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src/index.ejs'),
        title: 'My App'
      })
    ]
  };
};
